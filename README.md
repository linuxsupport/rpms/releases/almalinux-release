# almalinux-release RPM

The main branch of this repo is empty, all the fun happens in other branches:

 * [a8](https://gitlab.cern.ch/linuxsupport/rpms/releases/almalinux-release/-/tree/a8)
 * [a9](https://gitlab.cern.ch/linuxsupport/rpms/releases/almalinux-release/-/tree/a9)

